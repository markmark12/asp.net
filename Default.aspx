﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:countryConnectionString %>" SelectCommand="SELECT * FROM [Table]"></asp:SqlDataSource>
        <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource1">
         
            
                <HeaderTemplate>
                    <table>
                    <tr>
                        <th>
                            <asp:Label ID="name" runat="server" Text="name"></asp:Label> 
                        </th>
                        <th>
                            <asp:Label ID="capital" runat="server" Text="capital"></asp:Label>
                        </th>
                        <th>
                            <asp:Label ID="population" runat="server" Text="population"></asp:Label>
                        </th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("capital") %>'></asp:Label>
                        </td>
                    </tr>
                </ItemTemplate>
            <FooterTemplate>
                </table>
                
            </FooterTemplate>
            </asp:Repeater>
        

       
       
    
    </div>
    </form>
</body>
</html>
